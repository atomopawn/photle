Photle is an image guessing game.  Players are shown a zoomed-in slice of an
image and have six tries to guess the five letter word associated with the
image.  After each incorrect guess, the game reveals more of the image.

Photle was designed by Robert Marmorstein with contributions from his wife,
Beth Marmorstein, and his mother Donna Marmorstein.  It is released as an open
source project under the GPL 3.0 License.  

Feel free to extend it.

If you find Photle useful or would like to contribute patches, you can contact
Robert at marmorsteinrm AT longwood DOT edu or call (434)395-2185.
