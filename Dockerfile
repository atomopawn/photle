FROM python:3

MAINTAINER Robert Marmorstein "marmorsteinrm@longwood.edu"

COPY ./requirements.txt /Photle/requirements.txt
WORKDIR /Photle

RUN pip3 install -r requirements.txt

COPY src /Photle

ENTRYPOINT [ "python3" ]

CMD [ "/Photle/__init__.py" ]
