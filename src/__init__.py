# Copyright 2022 Robert Marmorstein, Beth Marmorstein, and Donna Marmorstein

# This file is part of Photle.

# Photle is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# Photle is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with
# Photle. If not, see <https://www.gnu.org/licenses/>.

from flask import Flask, request, url_for, render_template, redirect, flash
from werkzeug.utils import secure_filename
import hashlib
import base64
import os
import io
import random
from pimage import PImage
from datetime import date

md5 = hashlib.md5()

app = Flask(__name__)
app.secret_key = 'Photle<>Wordle'

images = [ "sentinel" ]

def setup():
    global images
    random.seed()
    images = [ "sentinel" ]
    path = os.getcwd()
    folder = os.path.join(path, "images")
    with os.scandir(folder) as ptr:
        for entry in ptr:
            if entry.name.endswith('.png') and entry.is_file():
                name = entry.name.split('.')[-2]
                unique_name = name.split('-')
                if len(unique_name) > 1:
                    name = unique_name[1]
                images += [PImage(name, "images/" + entry.name)]

@app.route("/")
def main():
    setup()
    return render_template("index.html", wordnum=0)

@app.route("/reset")
def reset():
    setup()
    return "Game reset.  Number of images: " + str(len(images))

@app.route("/start")
def start():
    setup()
    num = random.randrange(1, len(images))
    return str(num)

@app.route("/word/<wordnum>")
def word(wordnum):
    num = int(wordnum)
    if num >= len(images): 
        num = 0
    if num > 0:
        return render_template("index.html", wordnum=num)
    return redirect(url_for("main"))

@app.route("/game")
def game():
    return render_template("game.js", wordnum=num)

@app.route("/guess/<wordnum>/<myguess>", methods=["GET", "POST"])
def guess(wordnum, myguess):
    num = int(wordnum)
    if num >= len(images): 
        num = 1
    word = images[num].name
    if myguess.lower() == word.lower():
        return "True"
    return "False"

@app.route("/step/<wordnum>/<level>", methods=["GET", "POST"])
def step(wordnum,level):
    num = int(wordnum)
    if num >= len(images): 
        num = 1
    images[num].load()
    #print(repr(images[num]))

    width = images[num].width
    height = images[num].height
    data = images[num].image

    lev = int(level)

    cropped_width = width/40.0*(lev*(lev+1))
    cropped_height = height/40.0*(lev*(lev+1))

    x = int(width / 2 - cropped_width / 2)
    y = int(height / 2 - cropped_height / 2)

    rect = (x, y, x+int(cropped_width), y+int(cropped_height))
    
    # https://www.geeksforgeeks.org/python-pil-image-crop-method/
    cropped = data.crop( rect )
    bytes = io.BytesIO()
    cropped.save(bytes, format='PNG')
    return base64.b64encode(bytes.getvalue())

@app.route("/final/<wordnum>", methods=["GET", "POST"])
def final(wordnum):
    num = int(wordnum)
    if num >= len(images): 
        num = 1
    image = images[num].image

    bytes = io.BytesIO()
    image.save(bytes, format='PNG')
    return base64.b64encode(bytes.getvalue())

@app.route("/submit")
def submit_image():
    return render_template("upload.html")

@app.route("/upload", methods=["POST"])
def upload():
    global md5
    try:
        if not 'email' in request.form:
            print('No email')
            flash('Must enter an e-mail address')
            return render_template("upload.html")

        if not 'files[]' in request.files:
            print('No files')
            flash('No files found')
            return render_template("upload.html")

        email = request.form.get('email')
        files = request.files.getlist('files[]')


        print(email)
        print(files)

        credits = open('credits.txt', 'a')
        path = os.getcwd()
        folder = os.path.join(path, "uploads")

        print("Folder:", folder)
        if not os.path.isdir(folder):
            os.mkdir(folder)

        for file in files:
            print('Downloading', file.filename)
            if file and file.filename.endswith('.png'):
                filename = secure_filename(file.filename)
                fullpath = os.path.join(folder, filename)
                md5.update(str(file).encode('utf-8'))
                filename = md5.hexdigest() + '-' + filename
                fullpath = os.path.join(folder, filename)
                file.save(fullpath)
                flash('Successfully uploaded ' + filename + '!')
                today = date.today()
                credits.write(email + ',' + filename + ',' + str(today) + '\n')
    except Exception as e:
        print(e)
    finally:
        credits.close()

    return render_template("upload.html")

@app.route("/getword/<wordnum>", methods=["GET", "POST"])
def getword(wordnum):
    num = int(wordnum)
    if num >= len(images): 
        num = 1
    return images[num].name

if __name__=="__main__":
    setup()
    app.run(host='0.0.0.0', port=5000)
