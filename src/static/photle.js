/*
 * Copyright 2022 Robert Marmorstein, Beth Marmorstein, and Donna Marmorstein
 * This file is part of Photle.
 *
 * Photle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Photle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * Photle. If not, see <https://www.gnu.org/licenses/>.
 */

let level = 1;
let guesses = Array();
let wordnum = 0;

function begin(result) {
	 wordnum = result;

   $("#lose").hide();
   $("#win").hide();
   $("#warning").hide();
   $("#game").show();
	 $("#submission").submit(handle_guess);

   $("#number")[0].innerHTML = wordnum;

   $.post("/step/" + wordnum + "/1").done(update_image);
}

function setup(result) {
   wordnum = result;
	 if (wordnum == 0) {
		 $.get("/start").done(begin);
	 }
	 else {
	    begin(wordnum);
	 }
}

function update_image(result) {
   $("#photle")[0].src = "data:image/png;base64," + result;
}

function update_guesses(guess)
{
   guesses.push(guess);
   $("#guesses")[0].innerHTML = ""
   for (var i = guesses.length - 1; i >= 0; --i) {
      $("#guesses")[0].innerHTML += "<li>" + guesses[i] + "</li>";
      $("#photle")[0].scrollIntoView();
   }
}

function reveal_word(result)
{
	$("#hidden").hide();
	$("#hidden")[0].innerHTML = "The secret word was: " + result + ".<br/>";
}	

function check_guess(result)
{
   if (result == "True") {
      $("#win").show();
		  $("#win")[0].scrollIntoView();
      $("#win").click = function() {location.reload();};
      $.post("/final/" + wordnum).done(update_image);
      $("#lose").hide();
      return;
   }
   if (guesses.length >= 6) {
      $("#lose").show();
		  $("#lose")[0].scrollIntoView();
      $("#lose").click = location.reload;
		  $("#reveal").click(function() {
					$("#hidden").show();
					$("#hidden")[0].scrollIntoView();
				  $("#reveal").hide();
			});
      $.post("/final/" + wordnum).done(update_image);
		  $.post("/getword/" + wordnum).done(reveal_word);
      return;
   }
   ++level;
   $.post("/step/" + wordnum + "/" + level).done(update_image);
   $("form")[0].reset();
   $("#game").show();
   $("#word").focus();
   $("#photle")[0].scrollIntoView();
}

function handle_guess(e) {
   e.preventDefault();

   guess = $("#word").val();

   if (guess.length != 5) {
      $("#warning").show();
      $("#photle")[0].scrollIntoView();
      $("form")[0].reset();
      return;
   }
   else {
      /* If the previous guess gave a warning, hide it now */
      $("#warning").hide();
   }
   update_guesses(guess);
   $("#game").hide();
   $.get("/guess/" + wordnum + "/" + guess).done(check_guess);
}

function share() {
	let text = 'I solved Photle #' + wordnum + ' in ' + guesses.length + ' tries! (http://photle.marmorstein.org/word/' + wordnum + ')';
	/* "Stolen" from Semantle: https://gitlab.com/novalis_dt/semantle/-/blob/master/static/assets/js/semantle.js */
	const copied = ClipboardJS.copy(text);
	if (copied) {
		alert("Copied to clipboard!");
	}
	else {
		alert("Could not copy to clipboard.");
	}
}
