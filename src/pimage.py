# Copyright 2022 Robert Marmorstein, Beth Marmorstein, and Donna Marmorstein

# This file is part of Photle.

# Photle is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# Photle is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with
# Photle. If not, see <https://www.gnu.org/licenses/>.

from PIL import Image 

class PImage:
    next_number = 1
    def __init__(self, name, filename):
        self.id = PImage.next_number
        self.name = name
        self.filename = filename
        self.width = 0
        self.height = 0
        self.image = None
        self.loaded = False

        PImage.next_number = PImage.next_number + 1

    def load(self):
        if self.loaded:
            return
        self.image = Image.open(self.filename)
        self.width, self.height = self.image.size
        self.loaded = True

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name + "[" + str(self.id) + "]: " + self.filename + " (" + str(self.width) + ", " + str(self.height) + ")\n" + repr(self.image)

    def close(self):
        self.image.close()
